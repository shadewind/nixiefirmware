#ifndef NIXIE_UTILS_H_
#define NIXIE_UTILS_H_

#include <stdint.h>

#if BYTE_ORDER == LITTLE_ENDIAN

/**
 * Converts the specified 64-bit integer from host to network byte order.
 *
 * @param v  The value to convert.
 *
 * @return The converted value.
 */
uint64_t hton64(uint64_t v);

/**
 * Converts the specified 64-bit integer from network to host byte order.
 *
 * @param v  The value to convert.
 *
 * @return The converted value.
 */
uint64_t ntoh64(uint64_t v);

#else

#define hton64(v) (v)
#define ntoh64(v) (v)

#endif

#endif /* NIXIE_UTILS_H_ */