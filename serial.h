#ifndef NIXIE_SERIAL_H_
#define NIXIE_SERIAL_H_

/**
 * Initializes the serial module.
 */
 void serial_init(void);

 /**
  * Writes the specified data to the serial port.
  *
  * @param data  A pointer to the data.
  * @param len   The length of the data.
  */
 void serial_write(char *data, int len);

/**
  * Writes the specified data to the serial port as lines
  * inserting \r before every occurrence of \n.
  *
  * @param data  A pointer to the data.
  * @param len   The length of the data.
  */
void serial_printLines(char *data, int len);

#endif /* NIXIE_SERIAL_H_ */