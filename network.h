#ifndef NIXIE_NETWORK_H_
#define NIXIE_NETWORK_H_

#include <lwip/ip_addr.h>

/**
 * Initializes and starts the network system.
 */
void network_init(void);

/**
 * Returns the IP address.
 *
 * @return The IP address of the network interface.
 */
ip_addr_t network_getAddr();

#endif /* NIXIE_NETWORK_H_ */