#include "sntp.h"

#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>

#include <string.h>

#include <lwip/api.h>
#include <lwip/netbuf.h>

#include "clock.h"
#include "utils.h"

#define SNTP_PORT 123

#define LI_MASK			(0x03 << 6)
#define LI_LMH_61		(1 << 6)
#define LI_LMH_59		(2 << 6)
#define LI_ALARM		(3 << 6)

#define MODE_CLIENT		3
#define MODE_SERVER		4
#define MODE_BROADCAST	5

#define NTP_VERSION_4	(4 << 3)
#define NTP_VERSION_3	(3 << 3)

#define TIMESTAMP_UNIX_EPOCH (2208988800LL << 32)

typedef struct {
	uint8_t flags;
	uint8_t stratum;
	uint8_t pollInterval;
	uint8_t precision;
	uint32_t rootDelay;
	uint32_t rootDispersion;
	uint8_t referenceId[4];
	uint64_t refTimestamp;
	uint64_t origTimestamp;
	uint64_t recvTimestamp;
	uint64_t xmitTimestamp;
} SNTPHeader;

static char sntpHostname[SNTP_MAX_HOSTNAME_LEN + 1];
static int syncInterval = 60 * 60;
static int synced;
static uint64_t lastSyncTime;
static xSemaphoreHandle syncMutex;

uint64_t unixMillisToNtp(uint64_t millis)
{
	uint64_t timestamp = ((millis / 1000) << 32) | (((millis % 1000) << 32) / 1000);
	return timestamp + TIMESTAMP_UNIX_EPOCH;
}

uint64_t ntpToUnixMillis(uint64_t ntp)
{
	ntp -= TIMESTAMP_UNIX_EPOCH;
	return ((ntp >> 32) * 1000) + (((ntp & 0x00000000FFFFFFFF) * 1000) >> 32);
}

/*
 * The calculations in this function are taken from the SNTP RFC4330:
 *
 *    Originate Timestamp     T1   time request sent by client
 *    Receive Timestamp       T2   time request received by server
 *    Transmit Timestamp      T3   time reply sent by server
 *    Destination Timestamp   T4   time reply received by client
 * 
 *  The roundtrip delay d and system clock offset t are defined as:
 *
 *    d = (T4 - T1) - (T3 - T2)     t = ((T2 - T1) + (T3 - T4)) / 2.
 */
static void sntp_apply(uint64_t t1, uint64_t t2, uint64_t t3, uint64_t t4)
{
	//Convert from fixed point to double
	double ft1 = t1 / 4294967296.0;
	double ft2 = t2 / 4294967296.0;
	double ft3 = t3 / 4294967296.0;
	double ft4 = t4 / 4294967296.0;
	//This is our offset as a double
	double t = ((ft2 - ft1) + (ft3 - ft4)) / 2;
	
	clock_offset(t * 1000.0);
	synced = 1;
	lastSyncTime = clock_currentTime();
}

static int sntp_sync(void)
{
	int ret = -1;
	struct netbuf *outbuf = NULL;
	struct netbuf *inbuf = NULL;
	struct netconn *conn = NULL;
	uint64_t t1, t4;

	ip_addr_t addr;
	if (netconn_gethostbyname(sntpHostname, &addr) != ERR_OK)
		goto cleanup;

	outbuf = netbuf_new();
	if (outbuf == NULL)
		goto cleanup;

	SNTPHeader *header = (SNTPHeader *)netbuf_alloc(outbuf, sizeof(SNTPHeader));
	if (header == NULL)
		goto cleanup;

	memset(header, 0, sizeof(SNTPHeader));
	header->flags = NTP_VERSION_4 | MODE_CLIENT;
	
	conn = netconn_new(NETCONN_UDP);
	if (conn == NULL)
		goto cleanup;

	if (netconn_connect(conn, &addr, SNTP_PORT) != ERR_OK)
		goto cleanup;

	//T1 is the time at which the request was sent.
	t1 = unixMillisToNtp(clock_currentTime());
	header->xmitTimestamp = hton64(t1);
	if (netconn_send(conn, outbuf) != ERR_OK)
		goto cleanup;
	netbuf_delete(outbuf);
	outbuf = NULL;

	netconn_set_recvtimeout(conn, 2000);
	if (netconn_recv(conn, &inbuf) != ERR_OK)
		goto cleanup;
	//T4 is the time at which the response was received.
	t4 = unixMillisToNtp(clock_currentTime());

	if (netbuf_len(inbuf) < sizeof(SNTPHeader))
		goto cleanup;

	SNTPHeader serverHeader;
	netbuf_copy_partial(inbuf, &serverHeader, sizeof(SNTPHeader), 0);
	//Check that the originate timestamp equals the transmit timestamp
	//in the header we sent for at least a little bit of security.
	if(serverHeader.origTimestamp != header->xmitTimestamp)
		goto cleanup;
	ret = 0;
	sntp_apply(
		t1, ntoh64(serverHeader.recvTimestamp),
		ntoh64(serverHeader.xmitTimestamp), t4);

cleanup:
	netbuf_delete(outbuf);
	netbuf_delete(inbuf);
	netconn_delete(conn);
	return ret;
}

static void sntp_task(void *params)
{
	vTaskDelay(configTICK_RATE_HZ * 5);

	for (;;) {
		xSemaphoreTake(syncMutex, portMAX_DELAY);
		sntp_sync();
		xSemaphoreGive(syncMutex);
		vTaskDelay(configTICK_RATE_HZ * syncInterval);
	}
}

void sntp_init(void)
{
	syncMutex = xSemaphoreCreateMutex();
	sntp_setServer("ntp.lth.se");
	xTaskCreate(sntp_task, (signed portCHAR *)"sntp", 512, NULL, 5, NULL);
}

void sntp_setServer(const char *server)
{
	xSemaphoreTake(syncMutex, portMAX_DELAY);
	strncpy(sntpHostname, server, SNTP_MAX_HOSTNAME_LEN);
	sntpHostname[SNTP_MAX_HOSTNAME_LEN] = 0;
	xSemaphoreGive(syncMutex);
}

void sntp_getServer(char *dest)
{
	xSemaphoreTake(syncMutex, portMAX_DELAY);
	strncpy(dest, sntpHostname, SNTP_MAX_HOSTNAME_LEN);
	xSemaphoreGive(syncMutex);
}

void sntp_setSyncInterval(int interval)
{
	syncInterval = interval;
}

int sntp_getSyncInterval()
{
	return syncInterval;
}