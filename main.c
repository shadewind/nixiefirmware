#include <lpc17xx_gpio.h>
#include <lpc17xx_clkpwr.h>
#include <lpc17xx_pinsel.h>
#include <lpc17xx_emac.h>

#include <FreeRTOS.h>
#include <task.h>

#include <string.h>

#include "mdio.h"
#include "serial.h"
#include "network.h"
#include "sntp.h"
#include "ui.h"
#include "clock.h"
#include "tcpcmd.h"
#include "iap.h"
#include "nixie.h"
#include "ir.h"
#include "syscall.h"
#include "prop.h"

void setup(void)
{
	CLKPWR_ConfigPPWR(CLKPWR_PCONP_PCGPIO, ENABLE);

	syscall_init();
	serial_init();
	clock_init();
	network_init();
	sntp_init();
	ui_init();
	tcpcmd_init();
	nixie_init();
	ir_init();
	prop_loadPersistent();
	iprintf("Initialized\n");
}

int main(void)
{
	setup();
	vTaskStartScheduler();
	for (;;);
}

void vApplicationStackOverflowHook(xTaskHandle xTask, signed portCHAR *pcTaskName)
{
	for(;;);
}