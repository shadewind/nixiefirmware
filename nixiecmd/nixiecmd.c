#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define NIXIE_PORT 19191

int assembleMessage(int argc, char **argv, char **message)
{
	int charLength = 0;
	//First find out the total length of the string to send.
	for (int arg = 0; arg < argc; arg++)
		charLength += strlen(argv[arg]);

	//Now we can assemble it. Make room for spaces as well.
	int messageLength = charLength + argc - 1;
	char *buffer = malloc(messageLength);
	int bufferPos = 0;
	for (int arg = 0; arg < argc; arg++) {
		int i = 0;
		while (argv[arg][i] != 0)
			buffer[bufferPos++] = argv[arg][i++];

		//Don't insert space for last argument.
		if (arg != (argc - 1))
			buffer[bufferPos++] = ' ';
	}

	*message = buffer;
	return messageLength;
}

int main(int argc, char **argv)
{
	if (argc < 3) {
		fprintf(stderr, "Too few arguments.\n");
		return 1;
	}

	struct in_addr nodeAddr;
	if (!inet_aton(argv[1], &nodeAddr)) {
		fprintf(stderr, "Invalid IP address.\n");
		return 1;
	}

	struct sockaddr_in saddr;
	memset(&saddr, 0, sizeof(struct sockaddr_in));
	saddr.sin_len = sizeof(struct sockaddr_in);
	saddr.sin_family = AF_INET;
	saddr.sin_port = htons(NIXIE_PORT);
	saddr.sin_addr = nodeAddr;

	int sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (sock == -1) {
		fprintf(stderr, "Unable to crate socket!\n");
		return 1;
	}

	int broadcast = 1;
	if(setsockopt(sock, SOL_SOCKET, SO_BROADCAST, &broadcast, sizeof(int)) != 0) {
		fprintf(stderr, "Unable to set socket options.\n");
		return 1;
	}

	if (connect(sock, (struct sockaddr *)&saddr, sizeof(struct sockaddr_in)) != 0) {
		fprintf(stderr, "Unable to connect socket.\n");
		return 1;
	}

	char *message;
	int messageLength = assembleMessage(argc - 2, argv + 2, &message);
	if(send(sock, message, messageLength, 0) == -1) {
		fprintf(stderr, "Unable to send message.\n");
		return 1;
	}

	free(message);
}
