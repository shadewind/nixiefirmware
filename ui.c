#include "ui.h"

#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>

#include <time.h>
#include <stdlib.h>

#include "clock.h"
#include "nixie.h"
#include "network.h"

typedef void (*DisplayFunc)(void);

static void ui_task(void *params);
static void ui_displayTime(void);
static void ui_displayDate(void);
static void ui_displayAddrLeft(void);
static void ui_displayAddrRight(void);

static DisplayFunc modeTime[] = { ui_displayTime, ui_displayDate, NULL };
static DisplayFunc modeAddr[] = { ui_displayAddrLeft, ui_displayAddrRight, NULL };
static DisplayFunc *modes[] = { modeTime, modeAddr, NULL };

static xSemaphoreHandle mutex;
static int displayMode = 0;
static int displaySubMode = 0;
static char displayString[] = "        ";
static int enabled = 0;
static int backlightEnabled = 0;

void ui_task(void *params)
{
	ui_toggleEnable();
	for (;;) {
		vTaskDelay(portTICK_RATE_MS * 100);
		xSemaphoreTake(mutex, portMAX_DELAY);

		modes[displayMode][displaySubMode]();
		nixie_setDisplay(displayString);
	
		xSemaphoreGive(mutex);
	}
}

void ui_displayTime(void)
{
	struct tm tim;
    time_t now = time(NULL);
    localtime_r(&now, &tim);

    sniprintf(displayString, 9, "%02d,%02d,%02d", tim.tm_hour, tim.tm_min, tim.tm_sec);
}

void ui_displayDate(void)
{
	struct tm tim;
    time_t now = time(NULL);
    localtime_r(&now, &tim);

    int year = tim.tm_year + 1900;
    int month = tim.tm_mon + 1;
    sniprintf(displayString, 9, "%4d%02d%02d", year, month, tim.tm_mday);
}

void ui_displayAddrLeft(void)
{
	ip_addr_t addr = network_getAddr();
	sniprintf(displayString, 9, "%3d.%3d.", ip4_addr1(&addr), ip4_addr2(&addr));
}

void ui_displayAddrRight(void)
{
	ip_addr_t addr = network_getAddr();
	sniprintf(displayString, 9, ".%3d.%3d", ip4_addr3(&addr), ip4_addr4(&addr));
}

void ui_init(void)
{
	tzset();
	mutex = xSemaphoreCreateMutex();
	xTaskCreate(ui_task, (signed portCHAR *)"ui", 512, NULL, 3, NULL);
}

void ui_toggleDisplayMode(void)
{
	xSemaphoreTake(mutex, portMAX_DELAY);
	int next = displayMode + 1;
	if (modes[next] == NULL)
		next = 0;
	displayMode = next;
	xSemaphoreGive(mutex);
}

void ui_toggleDisplaySubMode(void)
{
	xSemaphoreTake(mutex, portMAX_DELAY);
	int next = displaySubMode + 1;
	if (modes[displayMode][next] == NULL)
		next = 0;
	displaySubMode = next;
	xSemaphoreGive(mutex);
}

void ui_toggleEnable(void)
{
	xSemaphoreTake(mutex, portMAX_DELAY);
	enabled = !enabled;
	if (enabled) {
		nixie_setBrightness(255);
		nixie_setBacklight(backlightEnabled ? 255 : 0);
	} else {
		nixie_setBrightness(0);
		nixie_setBacklight(0);
	}
	xSemaphoreGive(mutex);
}

void ui_toggleEnableBacklight(void)
{
	xSemaphoreTake(mutex, portMAX_DELAY);
	backlightEnabled = !backlightEnabled;
	if (enabled)
		nixie_setBacklight(backlightEnabled ? 255 : 0);
	xSemaphoreGive(mutex);
}

void ui_updateTimezone(void)
{
	xSemaphoreTake(mutex, portMAX_DELAY);
	tzset();
	xSemaphoreGive(mutex);
}