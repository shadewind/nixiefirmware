#include "mdio.h"

#include <lpc17xx_pinsel.h>
#include <lpc17xx_gpio.h>

#define PORT_MDIO	0
#define PIN_MDIO	3
#define PORT_MDC	0
#define PIN_MDC		2
#define PHY_ADDRESS 0x1 // TODO this should probably not be here

#define PORT_MDIO_BASE ((LPC_GPIO_TypeDef *) (LPC_GPIO_BASE + (0x20 * PORT_MDIO)))
#define PORT_MDC_BASE ((LPC_GPIO_TypeDef *) (LPC_GPIO_BASE + (0x20 * PORT_MDIO)))

void mdio_init(void)
{
	PINSEL_CFG_Type cfg;

	cfg.Portnum = PORT_MDIO;
	cfg.Funcnum = PINSEL_FUNC_0;
	cfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	cfg.Pinnum = PIN_MDIO;
	PINSEL_ConfigPin(&cfg);
	PORT_MDIO_BASE->FIODIR &= ~(1 << PIN_MDIO);

	cfg.Portnum = PORT_MDC;
	cfg.Funcnum = PINSEL_FUNC_0;
	cfg.OpenDrain = PINSEL_PINMODE_NORMAL;
	cfg.Pinnum = PIN_MDC;
	PINSEL_ConfigPin(&cfg);
	PORT_MDC_BASE->FIODIR |= (1 << PIN_MDC);
}

static void mdio_delay(void)
{
	//for (int i = 0; i < 200; i++);
}

static void mdio_clock(void)
{
	mdio_delay();
	PORT_MDC_BASE->FIOSET = (1 << PIN_MDC);
	mdio_delay();
	PORT_MDC_BASE->FIOCLR = (1 << PIN_MDC);
}

static void mdio_output(uint32_t data, int n)
{
	uint32_t d = data << (32 - n);
	for (int i = 0; i < n; i++, d <<= 1) {
		if (d & 0x80000000)
			PORT_MDIO_BASE->FIOSET = (1 << PIN_MDIO);
		else
			PORT_MDIO_BASE->FIOCLR = (1 << PIN_MDIO);

		mdio_clock();
	}
}

static uint16_t mdio_input(void)
{
	uint16_t data = 0;

	for (int i = 0; i < 16; i++) {
		mdio_clock();
		data <<= 1;
		data |= (PORT_MDIO_BASE->FIOPIN >> PIN_MDIO) & 0x01;
	}

	return data;
}

uint16_t mdio_read(uint8_t reg)
{
	PORT_MDIO_BASE->FIODIR |= (1 << PIN_MDIO);

	mdio_output(0xFFFFFFFF, 32);
	mdio_output(0x06, 4);
	mdio_output(PHY_ADDRESS, 5);
	mdio_output(reg, 5);

	PORT_MDIO_BASE->FIODIR &= ~(1 << PIN_MDIO);
	mdio_clock();

	return mdio_input();
}

void mdio_write(uint8_t reg, uint16_t value)
{
	PORT_MDIO_BASE->FIODIR |= (1 << PIN_MDIO);

	mdio_output(0xFFFFFFFF, 32);
	mdio_output(0x06, 4);
	mdio_output(PHY_ADDRESS, 5);
	mdio_output(reg, 5);
	mdio_output(0x02, 2);
	mdio_output(value, 16);

	PORT_MDIO_BASE->FIODIR &= ~(1 << PIN_MDIO);
}