#define NIXIE_UDPCMD_H_
#ifndef NIXIE_UDPCMD_H_

/**
 * Initializes the UDP command system.
 */
void udpcmd_init(void);

#endif /* NIXIE_UDPCMD_H_ */