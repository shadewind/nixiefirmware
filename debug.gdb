target remote | openocd -p -f openocd/config.cfg

set remote hardware-breakpoint-limit 6
set remote hardware-watchpoint-limit 4

#define hook-stop
#	mon cortex_m3 maskisr on
#end

#define hook-run
#	mon cortex_m3 maskisr off
#end

#define hook-continue
#	mon cortex_m3 maskisr off
#end
